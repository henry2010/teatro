<?php
namespace App\Http\Controllers;

//use App\Usuario;
use App\Entities\Reserva;
use App\Entities\Butaca;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\IngresarReservaForm;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Doctrine\ORM\EntityManagerInterface;
use App\Entities\Usuario;
use Illuminate\Support\Facades\Log;

class ReservaController extends Controller{
    
    use FormBuilderTrait;
    
    protected $em;
    
    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }
    
    /**
     * Carga la vista para crear una nueva reserva, si recibe una fecha verifica la disponibilidad de butacas para esa fecha
     * @param type $id
     * @param type $fecha
     * @return view
     */
    public function reserva($id, $fecha=null){
        $usuario = $this->em->getRepository('App\Entities\Usuario')->find($id);
        $form = $this->form(IngresarReservaForm::class, [
            'method' => 'POST',
            'route' => ['guardarReserva', $id],
        ]);
        
        $reservas = null;
        if(!$fecha == null){//si hay alguna fecha seleccionada comprobar disponibilidad de butacas y establecer el valor de los campos del formulario
            $form->getField('fecha')->setValue($fecha);
            $form->getField('butacas')->setValue(null);
            $reservas = $this->em->getRepository('App\Entities\Reserva')->findBy(['fecha'=>\DateTime::createFromFormat("Y-m-d", $fecha)]);
        }
        
        return view('reserva.ingresar', compact('form'), [
            'usuario' => $usuario,
            'reservas' => $reservas
        ]);
    }
    
    /**
     * Valida los datos de una reserva, crea una reserva y almacena los datos de la reserva en un archivo log y en la base de datos
     * @param Request $request
     * @param type $id
     * @return route
     */
    public function guardarReserva(Request $request, $id){
        $form = $this->form(IngresarReservaForm::class);
        
        $form->validate(['fecha' => 'required', 'butacas' => 'required'], [
            'fecha.required' => "Por favor seleccione una fecha",
            'butacas.required' => "Por favor seleccione al menos una butaca"
        ]);
        
        if(!$form->isValid()){
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $usuario = $this->em->getRepository('App\Entities\Usuario')->find($id);
        
        if($usuario == NULL){
            return redirect()->route('listarUsuarios');
        }
        
        $reserva = new Reserva();
        $reserva->setFecha(\DateTime::createFromFormat("Y-m-d", $request->get('fecha')));
        $reserva->setUsuario($usuario);
        $arrayButacas = explode(',', $request->get('butacas'));//obtener datos de butacas
        
        foreach ($arrayButacas as $butaca){
            $items = explode('-', $butaca);//separar fila y columna del dato
            $butaca = new Butaca($items[0], $items[1]);
            $reserva->addButaca($butaca);
        }
        
        $this->em->persist($reserva);
        $this->em->flush();
        
        //guardar en logs
        Log::info('Reserva guardada: ', [
            'idUsuario' => $usuario->getId(),
            'idReserva' => $reserva->getId(),
            'personas' => $reserva->getPersonas()
        ]);
        
        return redirect()->route('listarUsuarios');        
    }
    
    /**
     * Deveulve una vista con las reservas de un usuario seleccionado
     * @param type $idUser
     * @return view
     */
    public function listarReservasUsuario($idUser){
        //seleccionar usuario y sus reservas
        $usuario = $this->em->getRepository('App\Entities\Usuario')->find($idUser);
        $reservas = $this->em->getRepository('App\Entities\Reserva')->findBy(['usuario'=>$usuario]);
        
        return view('reserva.listarReservasUsuario', [
            'usuario' => $usuario,
            'reservas' => $reservas
        ]);
    }
    
    /**
     * Elimina la reserva de un usuario
     * @param type $idUser
     * @param type $idReserva
     * @return route
     */
    public function eliminarReserva($idUser, $idReserva){
        $reserva = $this->em->getRepository('App\Entities\Reserva')->find($idReserva);
        
        $this->em->remove($reserva);
        $this->em->flush();
        return redirect()->route('listarReservasUsuario', ['idUser' => $idUser]);
    }
    
    /**
     * Vista que muestra un gráfico con las butacas asignadas a la reserva de un usuario
     * @param type $idUser
     * @param type $idReserva
     * @param type $regresar
     * @return View
     */
    public function verButacasUsuario($idUser, $idReserva, $regresar = 0){
        $usuario = $this->em->getRepository('App\Entities\Usuario')->find($idUser);
        $reserva = $this->em->getRepository('App\Entities\Reserva')->find($idReserva);
        
        return view('reserva.verButacasUsuario', [
            'usuario' => $usuario,
            'reserva' => $reserva,
            'regresar' => $regresar //mostrar botón regresar
        ]);
    }
    
    /**
     * Muestra todas las reservas creadas en el sitio
     * @return view
     */
    public function verReservas(){
        $reservas = $this->em->getRepository('App\Entities\Reserva')->findAll();
        
        return view('reserva.listarTodas', [
            'reservas' => $reservas,
        ]);
    }
    
}