<?php
namespace App\Http\Controllers;

//use App\Usuario;
use App\Entities\Usuario;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Forms\IngresarUsuarioForm;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Doctrine\ORM\EntityManagerInterface;
use Kris\LaravelFormBuilder\FormBuilder;

class UsuarioController extends Controller{
    
    use FormBuilderTrait;
    
    protected $em;
    
    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    /**
     * carga la vista con el formulario de ingresas datos
     * @param FormBuilder $formBuilder
     * @return view
     */
    public function ingresar(FormBuilder $formBuilder){
        $form = $formBuilder->create('App\Forms\IngresarUsuarioForm', [
            'method' => 'POST',
            'route' => 'guardarUsuario'
        ]);
        //$form->setClientValidationEnabled(FALSE);
        return view('usuario.ingresar', compact('form'));
    }
    
    /**
     * Valida el formulario y guarda el usuario en la base de datos
     * @param Request $request
     * @return route
     */
    public function guardarUsuario(Request $request){
        $form = $this->form(IngresarUsuarioForm::class);
        
        $form->validate(['apellidos' => 'required', 'nombre' => 'required'], [
            'apellidos.required' => "Por favor complete el campo apellidos",
            'nombre.required' => "Por favor complete el campo nombre"
        ]);
        
        if(!$form->isValid()){
            $form->setErrorsEnabled(TRUE);
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $usuario = new Usuario();
        $usuario->setNombre($request->get('nombre'));
        $usuario->setApellidos($request->get('apellidos'));
        $usuario->setFecha_creacion(new \DateTime());
        $usuario->setFecha_actualizacion(new \DateTime());
        
        $this->em->persist($usuario);
        $this->em->flush();
        
        return redirect()->route('listarUsuarios');
    }
    
    /**
     * listar todos los usuarios registrados
     * @return view
     */
    public function listar(){
        $usuarios = $this->em->getRepository('App\Entities\Usuario')->findAll();
        
        return view('usuario.listar', ['usuarios' => $usuarios]);
    }
    
    /**
     * Carga la vista con el formulario pata editar los datos de un usuario
     * @param type $id
     * @return view
     */
    public function editar($id){
        $usuario = $this->em->getRepository('App\Entities\Usuario')->find($id);
        
        if($usuario == NULL){
            return redirect()->route('listarUsuarios');
        }
        
        $form = $this->form(IngresarUsuarioForm::class, [
            'method' => 'POST',
            'route' => ['editarUsuario',$id],
            'model' => $usuario,
        ]);
        
        $form->getField('nombre')->setValue($usuario->getNombre());
        $form->getField('apellidos')->setValue($usuario->getApellidos());
        
        return view('usuario.editar', compact('form'));
    }
    
    /**
     * Actualiza los datos de un usuario registrado
     * @param Request $request
     * @param type $id
     * @return route
     */
    public function editarUsuario(Request $request, $id){
        $form = $this->form(IngresarUsuarioForm::class);       
        
        if(!$form->isValid()){
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $usuario = $this->em->getRepository('App\Entities\Usuario')->find($id);
        $usuario->setNombre($request->get('nombre'));
        $usuario->setApellidos($request->get('apellidos'));
        $usuario->setFecha_actualizacion(new \DateTime());
        
        $this->em->flush();
        
        return redirect()->route('listarUsuarios');
    }

    /**
     * Elimina un usuario del sitio
     * @param type $id
     * @return route
     */
    public function eliminar($id){
        $usuario = $this->em->getRepository('App\Entities\Usuario')->find($id);
        
        if($usuario == NULL){
            return redirect()->route('listarUsuarios');
        }
        
        $this->em->remove($usuario);
        $this->em->flush();
        return redirect()->route('listarUsuarios');
    }
}