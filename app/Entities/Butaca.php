<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="t_butacas")
 */
class Butaca {

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $fila;
    
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $columna;
    
    /**
     * @ORM\ManyToOne(targetEntity="Reserva", inversedBy="butacas")
     * @var Reserva
     */
    private $reserva;
    
    public function __construct($fila, $columna) {
        $this->fila = $fila;
        $this->columna = $columna;
    }
    
    function getId() {
        return $this->id;
    }

    function getFila() {
        return $this->fila;
    }

    function getColumna() {
        return $this->columna;
    }

    function setFila($fila) {
        $this->fila = $fila;
    }

    function setColumna($columna) {
        $this->columna = $columna;
    }
    
    function getReserva() {
        return $this->reserva;
    }

    function setReserva(Reserva $reserva) {
        $this->reserva = $reserva;
    }
    
}