<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="t_reservas")
 */
class Reserva {

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $personas;
    
    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="reservas")
     * @var Usuario
     */
    private $usuario;
    
    /**
     * @ORM\OneToMany(targetEntity="Butaca", mappedBy="reserva", cascade={"remove", "persist"})
     * @var ArrayCollection|Butaca[]
     */
    private $butacas;
    
    public function __construct() {
        $this->butacas = new ArrayCollection();
    }
    
    function getId() {
        return $this->id;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getPersonas() {
        return $this->personas;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    function setPersonas($personas) {
        $this->personas = $personas;
    }
    
    function getUsuario() {
        return $this->usuario;
    }

    function setUsuario(Usuario $usuario) {
        $this->usuario = $usuario;
    }
    
    public function addButaca(Butaca $butaca){
        if(!$this->butacas->contains($butaca)){
            $butaca->setReserva($this);
            $this->butacas->add($butaca);
            $this->setPersonas($this->butacas->count());
        }        
    }
    
    public function getButacas(){
        return $this->butacas;
    }

}
