<?php
namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="t_usuarios")
 */

class Usuario
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nombre;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $apellidos;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_creacion;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_actualizacion;
    
    /**
     * @ORM\OneToMany(targetEntity="Reserva", mappedBy="usuario", cascade={"remove", "persist"})
     * @var ArrayCollection|Reserva[]
     */
    private $reservas;
    
    public function __construct() {
        $this->reservas = new ArrayCollection();
    }
    
    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
    }
    
    public function addReserva(Reserva $reserva){
        $this->reservas->add($reserva);
    }
    
    public function getReservas(){
        return $this->reservas;
    }
    
    function getFecha_creacion() {
        return $this->fecha_creacion;
    }

    function getFecha_actualizacion() {
        return $this->fecha_actualizacion;
    }

    function setFecha_creacion($fecha_creacion) {
        $this->fecha_creacion = $fecha_creacion;
    }

    function setFecha_actualizacion($fecha_actualizacion) {
        $this->fecha_actualizacion = $fecha_actualizacion;
    }

}