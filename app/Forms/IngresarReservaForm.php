<?php
namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class IngresarReservaForm extends Form 
{
    public function buildForm() {
        $this
                ->add('fecha', 'date')
                ->add('butacas', 'hidden')
                ->add("Guardar", "submit");
    }
}