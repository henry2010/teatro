<?php
namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class IngresarUsuarioForm extends Form 
{
    public function buildForm() {
        $this
                ->add('nombre', 'text', [
                    'label' => 'Nombre',
                ])
                ->add("apellidos", "text",[
                    'label' => 'Apellidos'
                ])
                ->add("Guardar", "submit");
    }
}