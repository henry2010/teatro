@extends('layouts.base')

@section('content')
<div class="col-10 offset-1">
    <div class="card">
        <div class="card-body">
            <h1 class="card-title">Usuarios Registrados</h1>
            <div class="row">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                            <th>Reservas</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($usuarios as $u)
                        <tr>
                            <td>{{ $u->getNombre() }}</td>
                            <td>{{ $u->getApellidos() }}</td>
                            <td><a href='/usuario/editar/{{ $u->getId() }}'>Editar</a></td>
                            <td><a href="/usuario/eliminar/{{ $u->getId() }}">Eliminar</a></td>
                            <td><a href="/reserva/listarReservasUsuario/{{ $u->getId() }}">Ver reservas</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-6">
                    <a href="/usuario/ingresar" class="btn btn-info">Agregar nuevo usuario</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

