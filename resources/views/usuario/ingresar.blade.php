@extends('layouts.base')

@section('content')
<div class="col-10 offset-1">
    <div class="card">
        <div class="card-body">
            <h1 class="card-title">Ingresar nuevo usuario</h1>
            <div class="col-6 offset-3">
                {!! form_start($form) !!}
                <div class="form-group">
                    {!! form_label($form->nombre) !!}
                    {!! form_widget($form->nombre) !!}
                    {!! form_errors($form->nombre, $options = ['attr' => ['class' => 'form-text text-muted']]) !!}
                </div>
                <div class="form-group">
                    {!! form_label($form->apellidos) !!}
                    {!! form_widget($form->apellidos) !!}
                    {!! form_errors($form->apellidos, $options = ['attr' => ['class' => 'form-text text-muted']]) !!}
                </div>
                <div class="form-group">
                    {!! form_widget($form->Guardar, $options = ['attr' => ['class' => 'btn btn-primary']]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

