@extends('layouts.base')

@section('content')
<div class="col-10 offset-1">
    <div class="card">
        <div class="card-body">
            <h1 class="card-title">Reservas del teatro</h1>
            <div class="row">
                @if(count($reservas) > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Fecha</th>
                            <th>No. personas</th>
                            <th>Butaca</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($reservas as $r)
                        <tr>
                            <td>{{ $r->getUsuario()->getNombre() }} {{ $r->getUsuario()->getApellidos() }}</td>
                            <td>{{ $r->getFecha()->format('Y-m-d') }}</td>
                            <td>{{ $r->getPersonas() }}</td>
                            <td>
                                <ul>
                                    @foreach($r->getButacas() as $butaca)
                                    <li>Fila: {{ $butaca->getFila() }} - Columna: {{ $butaca->getColumna() }}</li>
                                    @endforeach
                                </ul>
                                <a href="/reserva/verButacasUsuario/{{ $r->getUsuario()->getId() }}/{{ $r->getId() }}">Ver Gráfico</a>
                            </td>
                            <td><a href="/reserva/eliminarReservaUsuario/{{ $r->getUsuario()->getId() }}/{{ $r->getId() }}">Eliminar</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <div class="col-10 offset-1">
                    <div class="alert alert-info" role="alert">
                        <p>No hay ninguna reserva creada</p>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection