@extends('layouts.base')

@section('content')
<div class="col-10 offset-1">
    <div class="card">
        <div class="card-body">
            <h1 class="card-title">Detalle reserva usuario: {{ $usuario->getNombre() }} {{ $usuario->getApellidos() }}</h1>
            <h3>Fecha: {{ $reserva->getFecha()->format('Y-m-d') }}</h3>
            @for($i = 0; $i<5; $i++)
            <div class="row" style="margin-top: 20px;">
                @for($j = 1; $j<11; $j++)
                    @if($j == 1)
                        <div class="col-1 offset-1">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-info butaca" id="butaca_{{ $i+1 }}-{{ $j }}" data-role="{{ $i+1 }}-{{ $j }}">{{ $j + ($i*10) }}</button>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-1">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-info butaca" id="butaca_{{ $i+1 }}-{{ $j }}" data-role="{{ $i+1 }}-{{ $j }}">{{ $j + ($i*10) }}</button>
                                </div>
                            </div>
                        </div>
                    @endif
                @endfor
            </div>
            @endfor
            
            @if($regresar == 1)
            <div class="row" style="margin-top:20px;">
                <div class="col-6">
                    <a href="/reserva/listarReservasUsuario/{{ $usuario->getId() }}" class="btn btn-info">Regresar</a>
                </div>
            </div>
            @else
            <div class="row" style="margin-top:20px;">
                <div class="col-6">
                    <a href="/reserva/verReservas" class="btn btn-info">Volver a reservas</a>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script>
    $(document).ready(function () {
    @if($reserva != null)
        @foreach($reserva->getButacas() as $b)
        $('#butaca_'+{{ $b->getFila() }}+'-'+{{ $b->getColumna() }}).removeClass('btn-info');
        $('#butaca_'+{{ $b->getFila() }}+'-'+{{ $b->getColumna() }}).addClass('btn-danger');
        $('#butaca_'+{{ $b->getFila() }}+'-'+{{ $b->getColumna() }}).attr('disabled', 'true');
        @endforeach
    @endif
});
    </script>
@endsection

