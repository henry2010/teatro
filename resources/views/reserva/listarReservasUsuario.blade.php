@extends('layouts.base')

@section('content')
<div class="col-10 offset-1">
    <div class="card">
        <div class="card-body">
            <h1 class="card-title">Reservas del usuario {{ $usuario->getNombre() }} {{ $usuario->getApellidos() }}</h1>
            <div class="row">
                @if(count($reservas) > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>No. personas</th>
                            <th>Butaca</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($reservas as $r)
                        <tr>
                            <td>{{ $r->getFecha()->format('Y-m-d') }}</td>
                            <td>{{ $r->getPersonas() }}</td>
                            <td>
                                <ul>
                                    @foreach($r->getButacas() as $butaca)
                                    <li>Fila: {{ $butaca->getFila() }} - Columna: {{ $butaca->getColumna() }}</li>
                                    @endforeach
                                </ul>
                                <a href="/reserva/verButacasUsuario/{{ $usuario->getId() }}/{{ $r->getId() }}/1">Ver Gráfico</a>
                            </td>
                            <td><a href="/reserva/eliminarReservaUsuario/{{ $usuario->getId() }}/{{ $r->getId() }}">Eliminar</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <div class="col-10 offset-1">
                    <div class="alert alert-info" role="alert">
                        <p>El usuario seleccionado no tiene reservas</p>
                    </div>
                </div>
                @endif
            </div>
            <div class="row">
                <div class="col-6">
                    <a href="/reserva/ingresar/{{ $usuario->getId() }}" class="btn btn-info">Agregar nueva reserva</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


