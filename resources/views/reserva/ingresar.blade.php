@extends('layouts.base')

@section('content')
<div class="col-10 offset-1">
    <div class="card">
        <div class="card-body">
            <h1 class="card-title">Ingresar nueva reserva para el usuario {{ $usuario->getNombre() }} {{ $usuario->getApellidos() }}</h1>
            {!! form_start($form) !!}
            <div class="form-group">
                {!! form_label($form->fecha) !!}
                {!! form_widget($form->fecha, $options = ['attr' => ['id' => 'fecha']]) !!}
                {!! form_errors($form->fecha, $options = ['attr' => ['class' => 'form-text text-muted']]) !!}
            </div>
            {!! form_widget($form->butacas, $options = ['attr' => ['id' => 'butacas']]) !!}
            {!! form_errors($form->butacas, $options = ['attr' => ['class' => 'form-text text-muted']]) !!}
            <div class="form-group">
                <div class="alert alert-success" role="alert">
                    <label>Número de personas</label>
                    <span id="numPersonas">0</span>
                </div>
            </div>
            <div class="form-group">
                {!! form_widget($form->Guardar, $options = ['attr' => ['class' => 'btn btn-primary']]) !!}
            </div>
            {!! form_end($form) !!}

            <div class="row">
                <div class="col-10 offset-1">
                    <div class="alert alert-info" role="alert">
                        Seleccione una butaca
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-10 offset-1">
                    <div class="alert alert-danger" role="alert">
                        Butacas en este color no están disponibles
                    </div>
                </div>
            </div>
            @for($i = 0; $i<5; $i++)
            <div class="row" style="margin-top: 20px;">
                @for($j = 1; $j<11; $j++)
                    @if($j == 1)
                        <div class="col-1 offset-1">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-info butaca" id="butaca_{{ $i+1 }}-{{ $j }}" data-role="{{ $i+1 }}-{{ $j }}">{{ $j + ($i*10) }}</button>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-1">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-info butaca" id="butaca_{{ $i+1 }}-{{ $j }}" data-role="{{ $i+1 }}-{{ $j }}">{{ $j + ($i*10) }}</button>
                                </div>
                            </div>
                        </div>
                    @endif
                @endfor
            </div>
            @endfor
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script>
$(document).ready(function () {
    var butacas = [];
    $('.butaca').click(function(){
        $(this).removeClass("btn-info");
        $(this).addClass("btn-success");
        butacas.push($(this).attr('data-role'));
        $('#butacas').val(butacas.toString());
        $('#numPersonas').html(butacas.length);
    });
    $('#fecha').change(function(){
        window.location.replace('/reserva/ingresar/'+{{ $usuario->getId() }}+'/'+$(this).val());
    });
    
    @if($reservas != null)
        @foreach($reservas as $r)
            @foreach($r->getButacas() as $b)
            $('#butaca_'+{{ $b->getFila() }}+'-'+{{ $b->getColumna() }}).removeClass('btn-info');
            $('#butaca_'+{{ $b->getFila() }}+'-'+{{ $b->getColumna() }}).addClass('btn-danger');
            $('#butaca_'+{{ $b->getFila() }}+'-'+{{ $b->getColumna() }}).attr('disabled', 'true');
            @endforeach
        @endforeach
    @endif
});
    </script>
    @endsection

