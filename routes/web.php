<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], '/usuario/ingresar', 'UsuarioController@ingresar');
Route::match(['post'], '/usuario/guardarUsuario', 'UsuarioController@guardarUsuario')->name('guardarUsuario');
Route::match(['get'], '/', 'UsuarioController@listar')->name('listarUsuarios');
Route::match(['get', 'post'], '/usuario/editar/{id}', 'UsuarioController@editar');
Route::match(['post'], '/usuario/editarUsuario/{id}', [
    'as' => 'editarUsuario',
    'uses' => 'UsuarioController@editarUsuario'
]);
Route::match(['get'], '/usuario/eliminar/{id}', [
    'as' => 'eliminarUsuario',
    'uses' => 'UsuarioController@eliminar'
]);
Route::match(['get', 'post'], '/reserva/ingresar/{id}/{fecha?}', [
    'as' => 'reservaIngresar',
    'uses' => 'ReservaController@reserva'
]);
Route::match(['post'], '/reserva/guardarReserva/{id}', [
    'as' => 'guardarReserva',
    'uses' => 'ReservaController@guardarReserva'
]);
Route::match(['get'], '/reserva/listarReservasUsuario/{idUser}', [
    'as' => 'listarReservasUsuario',
    'uses' => 'ReservaController@listarReservasUsuario'
]);
Route::match(['get'], '/reserva/eliminarReservaUsuario/{idUser}/{idReserva}', [
    'as' => 'eliminarReservaUsuario',
    'uses' => 'ReservaController@eliminarReserva'
]);
Route::match(['get'], '/reserva/verButacasUsuario/{idUser}/{idReserva}/{regresar?}', [
    'as' => 'verButacasUsuario',
    'uses' => 'ReservaController@verButacasUsuario'
]);
Route::match(['get'], '/reserva/verReservas', [
    'as' => 'verReservas',
    'uses' => 'ReservaController@verReservas'
]);